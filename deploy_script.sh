#!/bin/bash
sudo yum -y install docker
sudo systemctl start docker
sudo chmod 777 //var/run/docker.sock
sudo docker ps 
echo "Set AWS Credential"
aws configure set default.region us-east-2; aws configure set aws_access_key_id '$AWS_ACCESS_KEY_ID' ; aws configure set aws_secret_access_key '$AWS_SECRET_ACCESS_KEY' ; aws ecr get-login | sudo sh
echo "Logging into ECR"
aws ecr get-login-password --region us-east-2 | docker login --username AWS --password-stdin 733863402243.dkr.ecr.us-east-2.amazonaws.com/riskitest
echo "Fetching Latest Image"
docker pull 733863402243.dkr.ecr.us-east-2.amazonaws.com/riskitest:latest
echo "Stopping current container"
docker stop test
echo "Removing old container"
docker rm -f test
echo "Rename the stopped container to old"
docker rename test test-old
echo "Starting new container"
docker run --name test -d -p 80:80 733863402243.dkr.ecr.us-east-2.amazonaws.com/riskitest:latest