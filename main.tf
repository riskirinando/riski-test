provider "aws" {
  access_key = "AKIAXPNZICZ2E2E4S2VY"
  secret_key = "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"
  region = "ap-southeast-1"
  profile = "default"
}

resource "aws_vpc" "vpc_test" { 
  cidr_block = "10.0.0.0/16" 
 
  tags = { 
    Environment = "vpc_test" 
  } 
} 

resource "aws_subnet" "private" {
  vpc_id     = aws_vpc.vpc_test.id 
  cidr_block = "10.0.1.0/24" 
  tags = { 
    Access = "private" 
  } 
} 
 
resource "aws_subnet" "public" {  
  vpc_id     = aws_vpc.vpc_test.id 
  cidr_block = "10.0.2.0/24" 
  tags = { 
    Access = "public" 
  } 
} 

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc_test.id 
 
} 
 
resource "aws_route_table" "public" {
  vpc_id = aws_vpc.vpc_test.id 
  route { 
    cidr_block = "0.0.0.0/0" 
    gateway_id = aws_internet_gateway.igw.id 
  }  
} 
 
resource "aws_route_table_association" "public" {
  subnet_id      = aws_subnet.public.id 
  route_table_id = aws_route_table.public.id 
} 
 
resource "aws_nat_gateway" "ngw" {
  allocation_id = aws_eip.nat.id 
  subnet_id = aws_subnet.public.id 
} 
 
resource "aws_route_table" "private" {
  vpc_id = aws_vpc.vpc_test.id 
  route { 
    cidr_block = "0.0.0.0/0" 
    nat_gateway_id = aws_nat_gateway.ngw.id
  } 
} 

resource "aws_eip" "nat" {
  vpc      = true 
} 
 
resource "aws_route_table_association" "private" {
  subnet_id = aws_subnet.private.id 
  route_table_id = aws_route_table.private.id 
} 

// Instance
resource "aws_instance" "test" {
ami = "ami-0615132a0f36d24f4"
instance_type = "t2.medium"
key_name = "test"
}

resource "aws_ebs_volume" "test" {
availability_zone = "ap-southeast-1a"
type = "gp2"
size = 33
tags = {
Name = "test_disk"
}
}

resource "aws_volume_attachment" "test" {
device_name = "/dev/xvdb"
volume_id   = "${aws_ebs_volume.test.id}"
instance_id = "${aws_instance.test.id}"
}


// Security Group

resource "aws_security_group" "allow_http" {
  name        = "allow_http"
  description = "Allow HTTP inbound connections"
  vpc_id = aws_vpc.vpc_test.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Allow HTTP Security Group"
  }

}

  //Alarm

  resource "aws_cloudwatch_metric_alarm" "foobar" {
  alarm_name                = "terraform-test-foobar5"
  comparison_operator       = "GreaterThanOrEqualToThreshold"
  evaluation_periods        = "2"
  metric_name               = "CPUUtilization"
  namespace                 = "AWS/EC2"
  period                    = "120"
  statistic                 = "Average"
  threshold                 = "45"
  alarm_description         = "This metric monitors ec2 cpu utilization"
  insufficient_data_actions = []
}
  // Auto Scaling Group
resource "aws_launch_configuration" "web" {
  name_prefix = "web-"

  image_id = "ami-0947d2ba12ee1ff75" 
  instance_type = "t2.micro"

  security_groups = [ aws_security_group.allow_http.id ]
  associate_public_ip_address = true
  }


resource "aws_security_group" "elb_http" {
  name        = "elb_http"
  description = "Allow HTTP traffic to instances through Elastic Load Balancer"
  vpc_id = aws_vpc.vpc_test.id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Allow HTTP through ELB Security Group"
  }
}

resource "aws_elb" "web_elb" {
  name = "web-elb"
  security_groups = [
    aws_security_group.elb_http.id
  ]
  subnets = [
    aws_subnet.private.id,
  ]

  cross_zone_load_balancing   = true

  health_check {
    healthy_threshold = 2
    unhealthy_threshold = 2
    timeout = 3
    interval = 30
    target = "HTTP:80/"
  }

  listener {
    lb_port = 80
    lb_protocol = "http"
    instance_port = "80"
    instance_protocol = "http"
  }

}

resource "aws_autoscaling_group" "web" {
  name = "web"

  min_size             = 2
  desired_capacity     = 2
  max_size             = 5
  
  health_check_type    = "ELB"
  load_balancers = [
    aws_elb.web_elb.id
  ]

  launch_configuration = aws_launch_configuration.web.name

  enabled_metrics = [
    "GroupMinSize",
    "GroupMaxSize",
    "GroupDesiredCapacity",
    "GroupInServiceInstances",
    "GroupTotalInstances"
  ]

  metrics_granularity = "1Minute"

  vpc_zone_identifier  = [
    aws_subnet.private.id
  ]

  lifecycle {
    create_before_destroy = true
  }

  tag {
    key                 = "Name"
    value               = "web"
    propagate_at_launch = true
  }
}

resource "aws_autoscaling_policy" "test" {
    name="test"
    autoscaling_group_name="web"
  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageCPUUtilization"
    }

    target_value = 45.0
  }

  
}
